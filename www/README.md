# Automad

*A file-based content management system and template engine*

[![Packagist](https://img.shields.io/packagist/v/automad/automad.svg)](https://packagist.org/packages/automad/automad)
![License](https://img.shields.io/packagist/l/automad/automad.svg)

---

## Documentation

The full documentation and feature list can be found at https://automad.org.   
The API reference is available at https://api.automad.org.   

## Text Editors Plugins

To make the development of themes more efficient, plugins providing syntax highlighting and snippets for Automad's template language are available for the following editors:

*	[Atom](https://atom.io/packages/language-automad)
* 	[Textmate 2](https://bitbucket.org/marcantondahmen/automad.tmbundle)

## Contributing

In case you are interested in contributing, you can help to grow a community, give feedback, report bugs or request features.    

However, I do not exclude at this point using parts of Automad's source in future projects under different licenses. In order to avoid having to ask anybody for permission when doing so, I will not accept any contributions to this repository. Please understand that pull requests will therefore be ignored.   

---
      
© 2013-2018 [Marc Anton Dahmen](https://marcdahmen.de)     
Released under the [MIT license](https://automad.org/license) 