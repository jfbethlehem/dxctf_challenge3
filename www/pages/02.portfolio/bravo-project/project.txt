title: Bravo / Project

-

tags: Bravo, Work

-

date: 2018-06-09 12:00:00

-

imageTeaser: ../*alpha*/landscape.png

-

imagesSlideshow: ../*alpha*/*.png

-

text: Place your main description text of your project here.

-

textTeaser: This page uses the other Project template of the Bravo theme. As with [Bravo / Post](/blog/bravo-post), all shown images are linked. Check out the [Images Slideshow](/dashboard?context=edit_page&url=%2Fportfolio%2Fbravo-project#data-and-settings) variable, to understand how links to images work.

-

theme: standard/bravo