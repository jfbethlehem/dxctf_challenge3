FROM ubuntu:16.04
RUN apt-get update && apt-get -y install git apache2
RUN cd /tmp && git clone http://bitbucket.org/jfbethlehem/dxctf_challenge3
RUN mv /tmp/dxctf_challenge3/www/* /var/www/html/
RUN rm -rf /var/www/html/.git
RUN /etc/init.d/apache2 restart
EXPOSE 80
